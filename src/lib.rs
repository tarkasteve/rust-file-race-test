

#[cfg(test)]
mod tests {
    extern crate tempdir;
    use self::tempdir::TempDir;
    use std::env;
    use std::fs;
    use std::fs::File;
    use std::io::Write;
    use std::path::{Path, PathBuf};
    extern crate rand;
    use self::rand::{thread_rng, Rng};

    /// Alt mktemp impl for comparison
    // fn mktemp() -> PathBuf {
    //     let mut rng = thread_rng();
    //     let dirname: String =  rng.gen_ascii_chars().take(5).collect();
    //     let mut path = PathBuf::new();
    //     path.push(format!("/tmp/{}", dirname));
    //     fs::create_dir(&path);
    //     path
    // }


    fn mktemp() -> PathBuf {
        TempDir::new("test").unwrap()
            .into_path()
    }

    #[test]
    fn create_single()
    {
        let path = mktemp();
        println!("TMPDIR is {:?}", path);

        env::set_current_dir(path).unwrap();
        let empty = Path::new("file.txt");
        {
            File::create(&empty).unwrap().flush().unwrap();
        }
        assert!(empty.exists());
    }


    #[test]
    fn create_multifile()
    {
        let path = mktemp();
        println!("TMPDIR is {:?}", path);

        env::set_current_dir(path).unwrap();
        let empty = Path::new("file.txt");
        let zzzz = Path::new("zzzz.txt");

        {
            File::create(&empty).unwrap()
                .flush().unwrap();

            let mut fd = File::create(&zzzz).unwrap();
            fd.write_all("zzzz".as_bytes()).unwrap();
            fd.flush().unwrap();
        }

        assert!(empty.exists());
        assert!(zzzz.exists());
    }
}
